<?php
require "../vendor/autoload.php";
use Nbphp\Request;
$request = Request::instance();
$domain = $request->domain();
$root_url = $request->rootUrl();
echo "<a href='{$domain}{$root_url}/index.php?s=/a/b/c/?p1=p1&p2=p2'>测试一</a><br>";
echo "<a href='{$domain}{$root_url}/index.php/a/b/c/p1/p1/p2/p2'>测试二</a><br>";

echo "当前module=====<br>{$request->module()}<br> ";
echo "当前controller=====<br>{$request->controller()}<br> ";
echo "当前action=====<br>{$request->action()}<br> ";


echo "当前访问域名或者IP=====<br>{$request->host()}<br> ";
echo "当前访问协议=====<br>{$request->scheme()}<br> ";
echo "当前访问的端口=====<br>{$request->port()}<br> ";
echo "当前请求的REMOTE_PORT=====<br>{$request->remotePort()}<br> ";
echo "当前请求的SERVER_PROTOCOL=====<br>{$request->protocol()}<br> ";
echo "当前请求的CONTENT_TYPE=====<br>{$request->contentType()}<br> ";
echo "当前包含协议的域名=====<br>{$request->domain()}<br> ";
echo "当前访问的子域名=====<br>{$request->subDomain()}<br> ";
echo "当前访问的泛域名=====<br>{$request->panDomain()}<br> ";
echo "当前访问的根域名=====<br>{$request->rootDomain()}<br> ";
echo "当前完整URL=====<br>{$request->url()}<br> ";
echo "当前URL（不含QUERY_STRING）=====<br>{$request->baseUrl()}<br> ";
echo "当前请求的QUERY_STRING参数=====<br>{$request->query()}<br> ";
echo "当前执行的文件=====<br>{$request->baseFile()}<br> ";
echo "URL访问根地址=====<br>{$request->root()}<br> ";
echo "URL访问根目录=====<br>{$request->rootUrl()}<br> ";
echo "当前请求URL的pathinfo信息（含URL后缀）=====<br>{$request->pathinfo()}<br> ";
echo "请求URL的pathinfo信息(不含URL后缀)=====<br>{$request->path()}<br> ";
echo "当前URL的访问后缀=====<br>{$request->ext()}<br> ";
echo "获取当前请求的时间=====<br>{$request->time()}<br> ";
echo "当前请求的资源类型=====<br>{$request->type()}<br> ";
echo "当前请求类型=====<br>{$request->method()}<br> ";

$_SESSION['test'] = 'session_test';
$_COOKIE['test'] = 'cookie_test';

echo "获取当前请求的变量=====<br>".json_encode((array)$request->param(),256)."<br> ";
echo "获取 GET 变量=====<br>".json_encode((array)$request->get(),256)."<br> ";
echo "获取 POST 变量=====<br>".json_encode((array)$request->post(),256)."<br> ";
echo "获取 PUT 变量=====<br>".json_encode((array)$request->put(),256)."<br> ";
echo "获取 DELETE 变量=====<br>".json_encode((array)$request->delete(),256)."<br> ";
echo "获取 SESSION 变量=====<br>".json_encode((array)$request->session(),256)."<br> ";
echo "获取 COOKIE 变量=====<br>".json_encode((array)$request->cookie(),256)."<br> ";
echo "获取 REQUEST 变量=====<br>".json_encode((array)$request->request(),256)."<br> ";
echo "获取 SERVER 变量=====<br>".json_encode((array)$request->server(),256)."<br> ";
echo "获取 ENV 变量=====<br>".json_encode((array)$request->env(),256)."<br> ";
echo "获取 路由（包括PATHINFO） 变量=====<br>".json_encode((array)$request->route(),256)."<br> ";
echo "获取 FILES 变量=====<br>".json_encode((array)$request->file(),256)."<br> ";

echo "判断是否GET请求=====<br>".($request->isGet()?'TRUE':'FALSE')."<br> ";
echo "判断是否POST请求=====<br>".($request->isPost()?'TRUE':'FALSE')."<br> ";
echo "判断是否PUT请求=====<br>".($request->isPut()?'TRUE':'FALSE')."<br> ";
echo "判断是否DELETE请求=====<br>".($request->isDelete()?'TRUE':'FALSE')."<br> ";
echo "判断是否AJAX请求=====<br>".($request->isAjax()?'TRUE':'FALSE')."<br> ";
echo "判断是否PJAX请求=====<br>".($request->isPjax()?'TRUE':'FALSE')."<br> ";
echo "判断是否为JSON请求=====<br>".($request->isJson()?'TRUE':'FALSE')."<br> ";
echo "判断是否手机访问=====<br>".($request->isMobile()?'TRUE':'FALSE')."<br> ";
echo "判断是否HEAD请求=====<br>".($request->isHead()?'TRUE':'FALSE')."<br> ";
echo "判断是否PATCH请求=====<br>".($request->isPatch()?'TRUE':'FALSE')."<br> ";
echo "判断是否OPTIONS请求=====<br>".($request->isOptions()?'TRUE':'FALSE')."<br> ";
echo "判断是否为CLI执行=====<br>".($request->isCli()?'TRUE':'FALSE')."<br> ";
echo "判断是否为CGI模式=====<br>".($request->isCgi()?'TRUE':'FALSE')."<br> ";

echo "获取 header 变量=====<br>".json_encode((array)$request->header(),256)."<br> ";